/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nolivier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/18 13:53:27 by nolivier          #+#    #+#             */
/*   Updated: 2019/04/05 10:11:20 by nolivier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MALLOC_H
# define MALLOC_H

# include <pthread.h>
# include <sys/mman.h>
# include <unistd.h>
# include <stdbool.h>

/*
**	getpagesize() = 4096
*/

# define PAGE_SIZE			getpagesize()

typedef struct				s_chunk
{
	struct s_chunk			*next;
	struct s_chunk			*next_free;
	size_t					align_size;
	size_t					size;
}							t_chunk;

typedef struct				s_chunk_large
{
	struct s_chunk_large	*next;
	struct s_chunk_large	*prev;
	size_t					align_size;
	size_t					size;
}							t_chunk_large;

typedef struct				s_map
{
	struct s_map			*next;
	struct s_map			*prev;
	t_chunk					*alloc;
	t_chunk					*free;
}							t_map;

typedef struct				s_pages
{
	t_map					*tiny;
	t_map					*small;
	t_chunk_large			*large;
}							t_pages;

extern t_pages				g_pages;
extern pthread_mutex_t		g_lock;

/*
**  Zone size        32
*/

# define T_MAP_SIZE			sizeof(t_map)
# define T_CHUNK_SIZE		sizeof(t_chunk)
# define T_CHUNK_LARGE_SIZE	sizeof(t_chunk_large)

/*
**	Tiny region			2 MB	2 097 152
**  512 * PAGE_SIZE
**	Allocation size		2048	(992 previous value)
**  define TINY_ALLOC_MAX     1024 - T_CHUNK_SIZE
*/

# define TINY_REGION		512 * PAGE_SIZE
# define TINY_ALLOC_MAX		2048
# define TINY_ALLOC_RESOL	16
# define TINY_MASK			TINY_ALLOC_RESOL - 1

/*
**	Small region		16 MB 	16 777 216
**  4096 * PAGE_SIZE
**	Allocation size		130 048
*/

# define SMALL_REGION		4096 * PAGE_SIZE
# define SMALL_ALLOC_MAX	130048
# define SMALL_ALLOC_RESOL	512
# define SMALL_MASK			SMALL_ALLOC_RESOL - 1

# define LARGE_ALLOC_RESOL	4096
# define LARGE_MASK			LARGE_ALLOC_RESOL - 1

size_t						ft_align(const size_t size, const size_t mask);
void						*malloc(size_t size);
void						*malloc_tiny(const size_t size);
void						*malloc_small(const size_t size);
void						free(void *ptr);
void						free_tiny_page(t_map *map, t_map *prev_map,
		void *ptr);
void						free_small_page(t_map *map, t_map *prev_map,
		void *ptr);
void						*realloc(void *ptr, size_t size);
void						*realloc_tiny(void *ptr, t_map *map, size_t size);
void						*realloc_small(void *ptr, t_map *map, size_t size);
void						*calloc(size_t count, size_t size);
void						*ft_memset(void *b, int c, size_t len);
void						ft_putnbr(int n);
void						ft_putnbr_hex(int n);
void						show_alloc_mem(void);
void						defrag(t_chunk *to_expand, t_chunk *to_delete);
void						add_free_list(t_map *map, t_chunk *chunk);
int							is_free(t_map *map, t_chunk *chunk);

#endif
