/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nolivier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/18 15:33:19 by nolivier          #+#    #+#             */
/*   Updated: 2019/04/04 14:52:52 by nolivier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdint.h>
#include <stdlib.h>
#include <malloc.h>

t_pages				g_pages = {0, 0, 0};
pthread_mutex_t		g_lock = PTHREAD_MUTEX_INITIALIZER;

static void			*malloc_large(const size_t size)
{
	t_chunk_large	*mem;
	size_t			tmp;

	tmp = ft_align(size + T_CHUNK_LARGE_SIZE, LARGE_MASK);
	if ((mem = mmap(0, tmp, PROT_READ | PROT_WRITE, MAP_ANON | MAP_PRIVATE,
					-1, 0)) == MAP_FAILED)
		return (NULL);
	mem->prev = NULL;
	if (g_pages.large)
	{
		mem->next = g_pages.large;
		(g_pages.large)->prev = mem;
	}
	else
		mem->next = NULL;
	g_pages.large = mem;
	mem->size = size;
	mem->align_size = tmp;
	return (mem + 1);
}

void				*malloc(size_t size)
{
	const size_t	type = (size > TINY_ALLOC_MAX) + (size > SMALL_ALLOC_MAX);
	void			*ptr;

	if ((long int)size < 0)
		return (NULL);
	pthread_mutex_lock(&g_lock);
	if (type == 0)
		ptr = malloc_tiny(size);
	else if (type == 1)
		ptr = malloc_small(size);
	else
		ptr = malloc_large(size);
	pthread_mutex_unlock(&g_lock);
	return (ptr);
}
