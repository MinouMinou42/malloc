/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nolivier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/18 15:33:19 by nolivier          #+#    #+#             */
/*   Updated: 2019/04/04 14:42:43 by nolivier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <malloc.h>

static void			free_large_page(void *ptr)
{
	t_chunk_large	*chunk;
	t_chunk_large	*prev;

	prev = NULL;
	chunk = g_pages.large;
	while (chunk && (void*)chunk < ptr)
	{
		if ((void *)chunk + T_CHUNK_SIZE == ptr)
		{
			if (g_pages.large == chunk)
				g_pages.large = chunk->next;
			else
				prev->next = chunk->next;
			if (chunk->next)
				chunk->next->prev = chunk->prev;
			munmap(chunk, chunk->align_size);
			break ;
		}
		prev = chunk;
		chunk = chunk->next;
	}
}

static t_map		*get_page_small(void *ptr, t_map *prev)
{
	t_map			*tmp;

	tmp = g_pages.small;
	prev = NULL;
	while (tmp)
	{
		if (ptr > (void*)tmp && ptr < ((void*)tmp + SMALL_REGION))
			return (tmp);
		prev = tmp;
		tmp = tmp->next;
	}
	return (NULL);
}

static t_map		*get_page_tiny(void *ptr, t_map *prev)
{
	t_map			*tmp;

	tmp = g_pages.tiny;
	while (tmp)
	{
		if (ptr > (void*)tmp && ptr < ((void*)tmp + TINY_REGION))
			return (tmp);
		prev = tmp;
		tmp = tmp->next;
	}
	return (NULL);
}

void				free(void *ptr)
{
	t_map			*map;
	t_map			*prev_map;

	if (!ptr || !(g_pages.tiny || g_pages.small || g_pages.large))
		return ;
	pthread_mutex_lock(&g_lock);
	prev_map = NULL;
	if ((map = get_page_tiny(ptr, prev_map)))
		free_tiny_page(map, prev_map, ptr);
	else if ((map = get_page_small(ptr, prev_map)))
		free_small_page(map, prev_map, ptr);
	else
		free_large_page(ptr);
	pthread_mutex_unlock(&g_lock);
}
