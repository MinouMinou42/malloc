/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   realloc_small.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nolivier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/18 13:12:28 by nolivier          #+#    #+#             */
/*   Updated: 2019/02/18 13:17:11 by nolivier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <malloc.h>

void				small_split(t_chunk *chunk, t_map *map, size_t size,
		size_t align_size)
{
	t_chunk			*free_chunk;

	free_chunk = (void*)chunk + T_CHUNK_SIZE + align_size;
	free_chunk->next = chunk->next;
	free_chunk->align_size = chunk->align_size - T_CHUNK_SIZE - align_size;
	chunk->next = free_chunk;
	add_free_list(map, free_chunk);
	chunk->size = size;
	chunk->align_size = align_size;
}

void				*small_manage(t_map *map, t_chunk *chunk, size_t size,
		void *ptr)
{
	void			*addr;
	size_t			align_size;

	if (size > TINY_ALLOC_MAX || size > chunk->align_size)
	{
		pthread_mutex_unlock(&g_lock);
		addr = malloc(size);
		memcpy(addr, ptr, size < chunk->size ? size : chunk->size);
		free(ptr);
		return (addr);
	}
	else if ((long int)(chunk->align_size - T_CHUNK_SIZE - (align_size =
					ft_align(size, SMALL_MASK))) >= 2 * SMALL_ALLOC_RESOL)
	{
		small_split(chunk, map, size, align_size);
		pthread_mutex_unlock(&g_lock);
		return (ptr);
	}
	else
	{
		chunk->size = size;
		pthread_mutex_unlock(&g_lock);
		return (ptr);
	}
}

void				*realloc_small(void *ptr, t_map *map, size_t size)
{
	t_chunk			*chunk;

	chunk = map->alloc;
	while (chunk)
	{
		if ((void*)chunk + T_CHUNK_SIZE == ptr)
			return (small_manage(map, chunk, size, ptr));
		chunk = chunk->next;
	}
	return (NULL);
}
