/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_punbr_hex.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nolivier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/18 13:23:08 by nolivier          #+#    #+#             */
/*   Updated: 2019/02/18 13:26:02 by nolivier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void		ft_putnbr_hex(int n)
{
	unsigned int	nb;
	char			c[1];
	char			*b;

	b = "0123456789abcdef";
	nb = n;
	if (n < 0)
	{
		write(1, "-", 1);
		nb = -nb;
	}
	if (nb > 15)
		ft_putnbr_hex(nb / 16);
	c[0] = *(b + nb % 16);
	write(1, c, 1);
}
