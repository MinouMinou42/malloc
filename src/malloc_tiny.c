/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc_tiny.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nolivier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/18 15:33:19 by nolivier          #+#    #+#             */
/*   Updated: 2019/02/18 13:01:19 by nolivier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <malloc.h>

static void			*init_tiny_chunk(t_map *mem, const size_t size)
{
	t_chunk			*ptr;
	t_chunk			*free_ptr;
	size_t			tmp;

	ptr = (void*)mem + T_MAP_SIZE;
	mem->alloc = ptr;
	ptr->size = size;
	tmp = ft_align(size, TINY_MASK);
	ptr->align_size = tmp;
	free_ptr = (void*)ptr + T_CHUNK_SIZE + tmp;
	free_ptr->align_size = TINY_REGION - tmp - T_MAP_SIZE - (2 * T_CHUNK_SIZE);
	free_ptr->next = NULL;
	ptr->next = free_ptr;
	mem->free = free_ptr;
	ptr->next_free = NULL;
	free_ptr->next_free = NULL;
	return (ptr + 1);
}

static void			*init_tiny(const size_t size)
{
	t_map			*mem;

	if ((mem = mmap(0, TINY_REGION, PROT_READ | PROT_WRITE,
					MAP_ANON | MAP_PRIVATE, -1, 0)) == MAP_FAILED)
		return (NULL);
	mem->prev = NULL;
	if (g_pages.tiny)
	{
		mem->next = g_pages.tiny;
		(g_pages.tiny)->prev = mem;
	}
	else
		mem->next = NULL;
	g_pages.tiny = mem;
	return (init_tiny_chunk(mem, size));
}

static void			tiny_split_chunk(t_map *map, t_chunk *cur, t_chunk *prev,
		const size_t align_size)
{
	t_chunk			*free_ptr;

	free_ptr = (void*)cur + align_size + T_CHUNK_SIZE;
	free_ptr->align_size = cur->align_size - align_size - T_CHUNK_SIZE;
	cur->align_size = align_size;
	free_ptr->next = cur->next;
	free_ptr->next_free = cur->next_free;
	cur->next = free_ptr;
	if (!prev)
	{
		map->free = free_ptr;
	}
	else
	{
		prev->next_free = free_ptr;
	}
}

static void			*chunk_tiny(t_map *map, t_chunk *cur, t_chunk *prev,
		const size_t size)
{
	size_t			tmp;

	if ((int)(cur->align_size - (tmp = ft_align(size, TINY_MASK)) -
				T_CHUNK_SIZE) >= TINY_ALLOC_RESOL)
		tiny_split_chunk(map, cur, prev, tmp);
	else
	{
		if (!prev)
			map->free = cur->next_free;
		else
			prev->next_free = cur->next_free;
	}
	cur->size = size;
	cur->next_free = NULL;
	return (cur + 1);
}

void				*malloc_tiny(const size_t size)
{
	t_map			*mem;
	t_chunk			*cur;
	t_chunk			*prev;

	mem = g_pages.tiny;
	while (mem)
	{
		prev = NULL;
		cur = mem->free;
		while (cur)
		{
			if (size <= cur->align_size)
				return (chunk_tiny(mem, cur, prev, size));
			prev = cur;
			cur = cur->next_free;
		}
		mem = mem->next;
	}
	return (init_tiny(size));
}
