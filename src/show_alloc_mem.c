/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   show_alloc_mem.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nolivier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/18 13:20:36 by nolivier          #+#    #+#             */
/*   Updated: 2019/04/04 16:28:28 by nolivier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <malloc.h>

static void			display_large(t_chunk_large *head, size_t *total)
{
	t_chunk_large	*chunk;

	chunk = head;
	write(1, "LARGE: 0x", 9);
	ft_putnbr_hex((int)head);
	write(1, "\n", 1);
	while (chunk)
	{
		write(1, "0x", 2);
		ft_putnbr_hex((int)((void*)chunk + T_CHUNK_SIZE));
		write(1, " - 0x", 5);
		ft_putnbr_hex((int)((void*)chunk + T_CHUNK_SIZE + chunk->size));
		write(1, ": ", 2);
		ft_putnbr((int)chunk->size);
		write(1, " octets\n", 8);
		*total += chunk->size;
		chunk = chunk->next;
	}
}

static void			display_chunk(t_chunk *chunk)
{
	write(1, "0x", 2);
	ft_putnbr_hex((int)((void*)chunk + T_CHUNK_SIZE));
	write(1, " - 0x", 5);
	ft_putnbr_hex((int)((void*)chunk + T_CHUNK_SIZE + chunk->size));
	write(1, ": ", 2);
	ft_putnbr((int)chunk->size);
	write(1, " octets | \n", 10);
}

static void			display_map_small(t_map *head, size_t *total)
{
	t_map			*map;
	t_chunk			*chunk;

	map = head;
	while (map)
	{
		chunk = map->alloc;
		write(1, "SMALL: 0x", 9);
		ft_putnbr_hex((int)map);
		write(1, "\n", 1);
		while (chunk)
		{
			if (!is_free(map, chunk))
			{
				display_chunk(chunk);
				ft_putnbr((int)ft_align(chunk->size, SMALL_MASK) +
						T_CHUNK_SIZE);
				write(1, " octets\n", 8);
				*total += chunk->size;
			}
			chunk = chunk->next;
		}
		map = map->next;
	}
}

static void			display_map_tiny(t_map *head, size_t *total)
{
	t_map			*map;
	t_chunk			*chunk;

	map = head;
	while (map)
	{
		chunk = map->alloc;
		write(1, "TINY: 0x", 8);
		ft_putnbr_hex((int)map);
		write(1, "\n", 1);
		while (chunk)
		{
			if (!is_free(map, chunk))
			{
				display_chunk(chunk);
				ft_putnbr((int)ft_align(chunk->size, TINY_MASK) + T_CHUNK_SIZE);
				write(1, " octets\n", 8);
				*total += chunk->size;
			}
			chunk = chunk->next;
		}
		map = map->next;
	}
}

void				show_alloc_mem(void)
{
	size_t			total;

	total = 0;
	pthread_mutex_lock(&g_lock);
	if (g_pages.tiny)
		display_map_tiny(g_pages.tiny, &total);
	if (g_pages.small)
		display_map_small(g_pages.small, &total);
	if (g_pages.large)
		display_large(g_pages.large, &total);
	if (total)
	{
		write(1, "Total: ", 7);
		ft_putnbr((int)total);
		write(1, " octets\n", 8);
	}
	pthread_mutex_unlock(&g_lock);
}
