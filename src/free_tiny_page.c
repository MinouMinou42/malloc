/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_tiny_page.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nolivier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/18 15:33:19 by nolivier          #+#    #+#             */
/*   Updated: 2019/02/18 11:59:53 by nolivier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <pthread.h>
#include <stdlib.h>
#include <malloc.h>

static int		verif_tiny(t_map *map, t_chunk *chunk, t_chunk *prev_chunk,
		void *ptr)
{
	chunk = map->alloc;
	while (chunk)
	{
		if ((void*)chunk + T_CHUNK_SIZE == ptr)
			return (1);
		prev_chunk = chunk;
		chunk = chunk->next;
	}
	return (0);
}

static void		tiny_unmap(t_map *map, t_map *prev_map)
{
	t_map		*ptr;
	size_t		nbr;

	(void)prev_map;
	ptr = g_pages.tiny;
	nbr = 0;
	while (ptr && nbr < 2)
	{
		++nbr;
		ptr = ptr->next;
	}
	if (nbr > 1)
	{
		if (g_pages.tiny == map)
			g_pages.tiny = map->next;
		else
		{
			map->prev->next = map->next;
			map->next->prev = map->prev;
		}
		munmap(map, TINY_REGION);
	}
}

static void		free_tiny_chunk(t_map *map, t_chunk *free_chunk, t_chunk *chunk)
{
	if (!map->free || (map->free != chunk && map->free > chunk))
	{
		chunk->next_free = map->free;
		map->free = chunk;
	}
	else
	{
		while (free_chunk && free_chunk->next_free &&
				free_chunk->next_free < chunk)
			free_chunk = free_chunk->next_free;
		chunk->next_free = free_chunk->next_free;
		free_chunk->next_free = chunk;
	}
}

void			free_tiny_page(t_map *map, t_map *prev_map, void *ptr)
{
	t_chunk		*chunk;
	t_chunk		*free_chunk;
	t_chunk		*prev_chunk;

	chunk = ptr - T_CHUNK_SIZE;
	prev_chunk = NULL;
	if (!verif_tiny(map, chunk, prev_chunk, ptr))
		return ;
	free_chunk = map->free;
	free_tiny_chunk(map, free_chunk, chunk);
	if ((void*)chunk + T_CHUNK_SIZE + chunk->align_size == chunk->next_free)
		defrag(chunk, chunk->next_free);
	if (free_chunk &&
			(void*)free_chunk + T_CHUNK_SIZE + free_chunk->align_size == chunk)
		defrag(free_chunk, chunk);
	if (map->alloc->next == NULL)
		tiny_unmap(map, prev_map);
}
