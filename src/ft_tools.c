/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tools.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nolivier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/23 15:21:42 by nolivier          #+#    #+#             */
/*   Updated: 2019/02/18 14:15:40 by nolivier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <malloc.h>

size_t			ft_align(const size_t size, const size_t mask)
{
	return ((size + mask) & ~mask);
}

void			defrag(t_chunk *to_expand, t_chunk *to_delete)
{
	to_expand->next_free = to_delete->next_free;
	to_expand->align_size += to_delete->align_size + T_CHUNK_SIZE;
	to_expand->next = to_delete->next;
}

static void		free_chunk(t_map *map, t_chunk *free_list, t_chunk *chunk)
{
	if (!map->free || (map->free != chunk && map->free > chunk))
	{
		chunk->next_free = map->free;
		map->free = chunk;
	}
	else
		while (free_list)
		{
			if ((!free_list->next_free && free_list != chunk) ||
					(free_list->next_free && free_list->next_free > chunk))
			{
				chunk->next_free = free_list->next_free;
				free_list->next_free = chunk;
				break ;
			}
			free_list = free_list->next_free;
		}
}

void			add_free_list(t_map *map, t_chunk *chunk)
{
	t_chunk		*free_list;

	free_list = map->free;
	free_chunk(map, free_list, chunk);
	if ((void*)chunk + T_CHUNK_SIZE + chunk->align_size == chunk->next_free)
		defrag(chunk, chunk->next_free);
	if (free_list && (void*)free_list + T_CHUNK_SIZE +
			free_list->align_size == chunk)
		defrag(free_list, chunk);
}

int				is_free(t_map *map, t_chunk *chunk)
{
	t_chunk		*tmp;

	tmp = map->free;
	while (tmp)
	{
		if (tmp == chunk)
			return (1);
		tmp = tmp->next_free;
	}
	return (0);
}
