/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nolivier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/18 15:33:19 by nolivier          #+#    #+#             */
/*   Updated: 2019/01/13 15:17:35 by nolivier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void				*malloc(size_t size)
{
	const size_t	type = (size > 64) + (size > 1024);
	void			*ptr;

	if ((long int)size < 0)
		return (0);
	if (type == 0)
		return (ptr);
	return (ptr);
}
