/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   realloc.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nolivier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/18 13:07:03 by nolivier          #+#    #+#             */
/*   Updated: 2019/04/04 14:59:06 by nolivier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <malloc.h>

static t_map		*get_page_tiny(void *ptr, t_map *prev)
{
	t_map			*tmp;

	tmp = g_pages.tiny;
	while (tmp)
	{
		if (ptr > (void*)tmp && ptr < ((void*)tmp + TINY_REGION))
			return (tmp);
		prev = tmp;
		tmp = tmp->next;
	}
	return (NULL);
}

static t_map		*get_page_small(void *ptr, t_map *prev)
{
	t_map			*tmp;

	tmp = g_pages.small;
	prev = NULL;
	while (tmp)
	{
		if (ptr > (void*)tmp && ptr < ((void*)tmp + SMALL_REGION))
			return (tmp);
		prev = tmp;
		tmp = tmp->next;
	}
	return (NULL);
}

t_chunk_large		*get_large(void *ptr)
{
	t_chunk_large	*tmp;

	tmp = g_pages.large;
	while (tmp)
	{
		if (ptr == (void*)tmp + T_CHUNK_SIZE)
			return (tmp);
		tmp = tmp->next;
	}
	return (0);
}

void				*large_realloc(void *ptr, t_chunk_large *chunk_large,
		size_t size)
{
	void			*addr;

	if (size > chunk_large->align_size || size <= SMALL_ALLOC_MAX)
	{
		pthread_mutex_unlock(&g_lock);
		addr = malloc(size);
		memcpy(addr, ptr, size < chunk_large->size ? size : chunk_large->size);
		free(ptr);
		return (addr);
	}
	else
	{
		chunk_large->size = size;
		pthread_mutex_unlock(&g_lock);
		return (ptr);
	}
}

void				*realloc(void *ptr, size_t size)
{
	t_map			*map;
	t_map			*prev_map;
	t_chunk_large	*chunk_large;
	void			*addr;

	if ((long int)size < 0)
		return (NULL);
	addr = 0;
	prev_map = NULL;
	pthread_mutex_lock(&g_lock);
	if (ptr && (map = get_page_tiny(ptr, prev_map)))
		addr = realloc_tiny(ptr, map, size);
	else if (ptr && (map = get_page_small(ptr, prev_map)))
		addr = realloc_small(ptr, map, size);
	else if (ptr && (chunk_large = get_large(ptr)))
		addr = large_realloc(ptr, chunk_large, size);
	else if (!ptr)
	{
		pthread_mutex_unlock(&g_lock);
		addr = malloc(size);
	}
	return (addr);
}
