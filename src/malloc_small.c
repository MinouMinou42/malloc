/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc_small.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nolivier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/18 15:33:19 by nolivier          #+#    #+#             */
/*   Updated: 2019/02/18 12:12:49 by nolivier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <malloc.h>

static void			*init_small_chunk(t_map *mem, const size_t size)
{
	t_chunk			*ptr;
	t_chunk			*free_ptr;
	size_t			tmp;

	ptr = (void*)mem + T_MAP_SIZE;
	mem->alloc = ptr;
	ptr->size = size;
	tmp = ft_align(size, SMALL_MASK);
	ptr->align_size = tmp;
	free_ptr = (void*)ptr + T_CHUNK_SIZE + tmp;
	free_ptr->align_size = SMALL_REGION - tmp - T_MAP_SIZE - (2 * T_CHUNK_SIZE);
	free_ptr->next = NULL;
	ptr->next = free_ptr;
	mem->free = free_ptr;
	ptr->next_free = NULL;
	free_ptr->next_free = NULL;
	return (ptr + 1);
}

static void			*init_small(const size_t size)
{
	t_map			*mem;

	if ((mem = mmap(0, SMALL_REGION, PROT_READ | PROT_WRITE,
					MAP_ANON | MAP_PRIVATE, -1, 0)) == MAP_FAILED)
		return (NULL);
	mem->prev = NULL;
	if (g_pages.small)
	{
		mem->next = g_pages.small;
		(g_pages.small)->prev = mem;
	}
	else
		mem->next = NULL;
	g_pages.small = mem;
	return (init_small_chunk(mem, size));
}

static void			small_split_chunk(t_map *map, t_chunk *cur, t_chunk *prev,
		const size_t align_size)
{
	t_chunk			*free_ptr;

	free_ptr = (void*)cur + align_size + T_CHUNK_SIZE;
	free_ptr->align_size = cur->align_size - align_size - T_CHUNK_SIZE;
	cur->align_size = align_size;
	free_ptr->next = cur->next;
	free_ptr->next_free = cur->next_free;
	cur->next = free_ptr;
	if (!prev)
	{
		map->free = free_ptr;
	}
	else
	{
		prev->next_free = free_ptr;
	}
}

static void			*chunk_small(t_map *map, t_chunk *cur, t_chunk *prev,
		const size_t size)
{
	size_t			align;

	if ((int)(cur->align_size - (align = ft_align(size, SMALL_MASK)) -
				T_CHUNK_SIZE) >= 2 * SMALL_ALLOC_RESOL)
		small_split_chunk(map, cur, prev, align);
	else
	{
		if (!prev)
			map->free = cur->next_free;
		else
			prev->next_free = cur->next_free;
	}
	cur->size = size;
	cur->next_free = NULL;
	return (cur + 1);
}

void				*malloc_small(const size_t size)
{
	t_map			*mem;
	t_chunk			*cur;
	t_chunk			*prev;

	mem = g_pages.small;
	while (mem)
	{
		prev = NULL;
		cur = mem->free;
		while (cur)
		{
			if (size <= cur->align_size)
				return (chunk_small(mem, cur, prev, size));
			prev = cur;
			cur = cur->next_free;
		}
		mem = mem->next;
	}
	return (init_small(size));
}
