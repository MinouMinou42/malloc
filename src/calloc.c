/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   calloc.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nolivier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/18 11:48:07 by nolivier          #+#    #+#             */
/*   Updated: 2019/02/18 11:50:19 by nolivier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <malloc.h>

void			*calloc(size_t count, size_t size)
{
	void		*ptr;

	ptr = malloc(size * count);
	if (ptr)
		memset(ptr, 0, size * count);
	return (ptr);
}
