.PHONY: all clean fclean name re

ifeq ($(HOSTTYPE),)
    HOSTTYPE := $(shell uname -m)_$(shell uname -s)
endif

NAME= libft_malloc_$(HOSTTYPE).so

CC= gcc

CFLAGS= -Wall -Wextra -Werror -g3

CPATH= src/

HPATH= inc/

OPATH= objs/

HFILES= inc/malloc.h 

INC= $(addprefix -I , $(HPATH))

LIB= libft/libft.a

CFILES= malloc.c \
		malloc_tiny.c \
		malloc_small.c \
		realloc.c \
		realloc_tiny.c \
		realloc_small.c \
		calloc.c \
		free.c \
		free_tiny_page.c \
		free_small_page.c \
		ft_tools.c \
		ft_memset.c \
		ft_putnbr.c \
		ft_putnbr_hex.c \
		show_alloc_mem.c
        # show_alloc_mem_ex.c

COBJ= $(addprefix $(OPATH), $(OFILES))

OFILES= $(CFILES:.c=.o)

all: COMP $(NAME)

COMP:
	mkdir -p $(OPATH)

$(NAME): $(COBJ)
	$(CC) $(CFLAGS) $(COBJ) $(INC) -shared -o $(NAME)
	ln -fs $(NAME) libft_malloc.so

$(OPATH)%.o: $(CPATH)%.c $(HFILES)
	$(CC) $(CFLAGS) $(INC) $< -c -o $@

clean:
	rm -rf $(COBJ)

fclean: clean
	rm -rf $(OPATH)
	rm -rf $(NAME)
	rm -rf libft_malloc.so

re: fclean all
